variable "ssh_key" {
  default = "ssh-rsa ..."
}
variable "proxmox_host" {
    default = "proxmox-host"
}
variable "template_name" {
    default = "debian-bullseye-template"
}
variable "root_size" {
    default = "50G"
}
variable "srv_size" {
    default = "10G"
}
variable "root_storage" {
    default = "local-lvm"
}
variable "srv_storage" {
    default = "local-lvm"
}
variable "admin_user" {
    default = "admin@pve"
}
variable "admin_pw" {
    default = "password"
}
variable "proxmox_api_url" {
    default =  "https://hostname:8006/api2/json"
}
