terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = "2.9.10"
    }
  }
}
provider "proxmox" {
  pm_api_url = var.proxmox_api_url
  pm_user = var.admin_user
  pm_password = var.admin_pw
  pm_tls_insecure = true

  pm_log_enable = true
  pm_log_file   = "terraform-plugin-proxmox.log"
  pm_debug      = true
  pm_log_levels = {
    _default    = "debug"
    _capturelog = ""
  }
}

resource "proxmox_vm_qemu" "test_server" {
  count = 1 
  name = "test-vm-${count.index + 1}" 
  target_node = var.proxmox_host
  clone = var.template_name
  agent = 0
  os_type = "cloud-init"
  cores = 4
  sockets = 2
  cpu = "host"
  memory = 8192
  scsihw = "virtio-scsi-pci"
  bootdisk = "scsi0"

  disk {
    size = var.root_size
    type = "scsi"
    storage = var.root_storage
    iothread = 1
  }

 disk {
    size = var.srv_size
    type = "scsi"
    storage = var.srv_storage
    iothread = 1
  }

  network {
    model = "virtio"
    bridge = "vmbr1"
    firewall  = false
    link_down = false
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  ipconfig0 = "ip=172.16.16.1${count.index + 1}/24,gw=172.16.16.254"

  sshkeys = <<EOF
  ${var.ssh_key}
  EOF

}
