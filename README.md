# terraform-proxmox



## Configuração do template Debian padrão no Proxmox

```
cd /tmp

wget -c https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2

qm create 9999 --memory 2048 --net0 virtio,bridge=vmbr1
qm importdisk 9999 debian-11-generic-amd64.qcow2 local
qm set 9999 --scsihw virtio-scsi-pci --scsi0 local:9999/vm-9999-disk-0.raw
qm set 9999 --ide2 local:cloudinit
qm set 9999 --boot c --bootdisk scsi0
qm set 9999 --serial0 socket --vga serial0 
qm set 9999 --sshkey /tmp/id_rsa.pub 
qm set 9999 --ciuser suporte
qm set 9999 --machine q35
qm template 9999
```
